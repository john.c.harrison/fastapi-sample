# Dev Setup

1. Ensure Python is installed >= 3.8 

```bash
$ python --version
```

2. Create a virtual environment.

```bash
python -m venv <env-name>
```

3. Activate newly created virtual environment

- Windows:

    ```powershell
    <env-name>\Scripts\activate.bat
    ```

- Linux/Bash:

    ```bash
    source <env-name>/bin/activate
    ```

4. Install the requirements from `requirements.txt`

```bash
pip install -r requirments.txt
```

5. Run the app

```python
uvicorn main:app --reload
```