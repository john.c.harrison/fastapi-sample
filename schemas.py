from pydantic import BaseModel
from fastapi_utils.api_model import APIMessage, APIModel
from typing import Union

class ItemBase(APIModel):
    name: str
    price: float
    is_offer: Union[bool, None] = None

class ItemCreate(ItemBase):
    pass

class Item(ItemBase):
    id: int
