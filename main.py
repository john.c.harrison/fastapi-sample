from fastapi import Depends, FastAPI, HTTPException
from sqlalchemy.orm import Session
import crud, models, schemas
from database import SessionLocal, engine
from fastapi_utils.cbv import cbv
from fastapi_utils.inferring_router import InferringRouter

models.Base.metadata.create_all(bind=engine)

app = FastAPI()
router = InferringRouter()

# Dependency
def get_db():
    db = SessionLocal()
    try:
        yield db
    finally:
        db.close()

@cbv(router)
class ItemView:

    session: Session = Depends(get_db)

    @router.get("/items")
    def list(self, skip: int = 0, limit: int = 100):
        items = crud.get_items(self.session, skip=skip, limit=limit)
        return items

    @router.get("/items/{item_id}")
    def retrieve(self, item_id: int):
        item = crud.get_item(self.session, item_id)
        return item
    
    @router.post("/items")
    def create(self, item: schemas.ItemCreate):
        return crud.create_item(self.session, item)

app.include_router(router)