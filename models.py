from sqlalchemy import Column, Integer, String, Float, Boolean
from database import Base

class Item(Base):
    __tablename__ = 'items'

    id = Column(Integer, primary_key=True)
    name = Column(String(100))
    price = Column(Float)
    is_offer = Column(Boolean)

    def __repr__(self):
        return f"Item(id={self.id}, name={self.name}, price={self.price}, is_offer={self.is_offer})"